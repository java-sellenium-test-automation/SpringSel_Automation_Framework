package com.mehdinuruzade.springsel.springsel_web.config;


import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;
import com.mehdinuruzade.springsel.springsel_web.annotations.WebDriverScopeBean;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.*;

@LazyConfiguration
@Profile("!remote")
public class WebDriverConfig {


    @WebDriverScopeBean
    @ConditionalOnProperty(name = "browser",havingValue = "firefox")
    public org.openqa.selenium.WebDriver firefoxDriver(){
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

    @WebDriverScopeBean
    @ConditionalOnMissingBean
    public org.openqa.selenium.WebDriver chromeDriver(){
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }



}
