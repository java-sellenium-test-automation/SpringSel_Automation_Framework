package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.Cards;
import com.mehdinuruzade.springsel.springsel_web.annotations.AzeLangConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@AzeLangConfiguration
public class AzCards extends Cards {

    @Override
    public List<String> TAMCARD_MASTERCARD_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-mastercard-standard-paypass-debet","TamKart MasterCard Debet"));
    }

    @Override
    public List<String> TAMCARD_VISACLASSIC_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-visa-classic-paywave-debet","TamKart VISA Classic Debet"));
    }

    @Override
    public List<String> TAMCJR_VISA_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamgenc-debet","TamGənc VISA Debet"));
    }

    @Override
    public List<String> TAMECO_VISACLASSIC_DEBIT() {
        return new  ArrayList<>(Arrays.asList("ferdi/kartlar/tam-eco", "TamEco VISA Classic Debet")) ;

    }

    @Override
    public List<String> TAM_DIGITAL() {
        return new ArrayList<>(Arrays.asList("tam-digicard", "Tam DigiCard"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_GOLD_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-mastercard-gold-paypass-debet", "TamKart MasterCard Gold Debet"));
    }

    @Override
    public List<String> TAMCARD_VISA_GOLD_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-visa-gold-paywave-debet", "TamKart VISA Gold Debet"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_PLATINUM_DEBIT() {
         return new ArrayList<>(Arrays.asList("tamkart-platinum-paypass-debet", "TamKart MasterCard Platinum Debet"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_PREMIUM_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-mastercard-premium-paypass-debet", "TamKart MasterCard Premium Debet"));
    }

    @Override
    public List<String> TAMCARD_VISA_PREMIUM_DEBIT() {
        return new ArrayList<>(Arrays.asList("tamkart-visa-premium-paywave-debet", "TamKart VISA Premium Debet"));
    }

    @Override
    public List<String> TAMCARD_VISA_CLASSIC() {
        return new ArrayList<>(Arrays.asList("tamkart-visa-classic","TamKart VISA Classic"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_STANDARD() {
        return new ArrayList<>(Arrays.asList("tamkart-mastercard-standard","TamKart MasterCard Standard"));
    }

    @Override
    public List<String> TAMCARD_VISA_PLATINUM() {
        return new ArrayList<>(Arrays.asList("tamkart-visa-platinum","TamKart VISA Platinum"));
    }

    @Override
    public List<String> ABB_MILES_VISA_CREDIT() {
        return  new ArrayList<>(Arrays.asList("abb-miles-visa-kredit","ABB Miles VISA Kredit"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_PLATINUM() {
        return new ArrayList<>(Arrays.asList("tamkart-platinum","TamKart MasterCard Platinum"));
    }

    @Override
    public List<String> TAMCARD_MASTERCARD_GOLD() {
        return new ArrayList<>(Arrays.asList("tamkart-mastercard-gold","TamKart MasterCard Gold"));
    }

    @Override
    public List<String> ABB_MILES_VISA_PREMIUM_CREDIT() {
        return new ArrayList<>(Arrays.asList("abb-miles-visa-premium-kredit","ABB Miles VISA Premium Kredit"));
    }

    @Override
    public List<String> ABB_MILES_MASTERCARD_PREMIUM_CREDIT() {
        return new ArrayList<>(Arrays.asList("abb-miles-premium-kredit","ABB Miles MasterCard Premium Kredit"));
    }


}
