package com.mehdinuruzade.springsel.data.languages;


public abstract class MainPageData extends BasePageData {




    @Override
    public abstract String pageTitle();
    public abstract String urlSuffix();

}
