package com.mehdinuruzade.springsel.pages.Main;

import com.mehdinuruzade.springsel.pages.Bank24.Bank24Page;
import com.mehdinuruzade.springsel.pages.BaseComponent;
import com.mehdinuruzade.springsel.pages.Cards.CardsPage;
import com.mehdinuruzade.springsel.pages.Mortgage.InternalMortgagePage;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.PageFragment;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

@PageFragment
public class MenuNavigation extends BaseComponent {

    //Beans
    @LazyAutoWired
    private CardsPage cardsPage;

    @LazyAutoWired
    private Bank24Page bank24Page;

    @LazyAutoWired
    private InternalMortgagePage internalMortgagePage;



    //Web Elements

         //Menu Items
    @FindBy(css = "ul>li:nth-child(4)>span>a")
    private WebElement cardsBtn;

    @FindBy(css = "ul>li:nth-child(8)>span>a")
    private WebElement bank24Btn;

    @FindBy(css = "ul>li:nth-child(2)>span>a")
    private WebElement mortgageBtn;


         //Sub-Menu Items
    @FindBy(css="ul>li:nth-child(2)>div>div>div:nth-child(1)>span:nth-child(1)>a")
    private WebElement intMortgageBtn;




    //Methods
    public CardsPage toCardsPage(){
        closeModal();
        this.cardsBtn.click();
        return cardsPage;
    }

    public Bank24Page toBank24Page(){
        closeModal();
        bank24Btn.click();
        return bank24Page;
    }

    public InternalMortgagePage toInternalMortgagePage(){
        closeModal();
        wait.until(ExpectedConditions.elementToBeClickable(mortgageBtn));
        actions.moveToElement(mortgageBtn).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(intMortgageBtn));
        intMortgageBtn.click();
        return internalMortgagePage;

    }
}
