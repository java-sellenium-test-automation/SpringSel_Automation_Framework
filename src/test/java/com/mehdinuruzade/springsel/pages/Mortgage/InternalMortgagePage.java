package com.mehdinuruzade.springsel.pages.Mortgage;


import com.mehdinuruzade.springsel.data.languages.InternalMortgagePageData;
import com.mehdinuruzade.springsel.pages.BasePage;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

@Page
public class InternalMortgagePage extends BasePage {

    @LazyAutoWired
    private InternalMortgagePageData data;


    //Web Elements
    @FindBy(css = "h1")
    WebElement internalMortgageHeader;

    @FindBy(css = "label[for='from_abb_no']")
    WebElement formNoBtn;

    @FindBy(css = "input#from_abb_no")
    WebElement formNoRadio;



    @Override
    public InternalMortgagePage verifyPage() {
        closeModal();
        Assert.assertEquals(internalMortgageHeader.getText(),data.pageHeader());
        if(internalMortgageHeader.getText().equals(data.pageHeader())){
            return this;
        }

        Assert.fail();
        return null;
    }


    public InternalMortgagePage clickFormNoButton(){
        closeModal();
        wait.until(ExpectedConditions.elementToBeClickable(formNoBtn));
        formNoBtn.click();
        System.out.println(formNoRadio.isSelected());
        return this;
    }

    public InternalMortgagePage formNoButtonIsSelected(){

        wait.until(ExpectedConditions.elementToBeSelected(formNoRadio));
        Assert.assertTrue(formNoRadio.isSelected());
        if (formNoRadio.isSelected()){
            return this;
        }

        Assert.fail();
        return null;

    }
}
