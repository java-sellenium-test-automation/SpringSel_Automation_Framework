package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.InternalMortgagePageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;


//@AzeLangConfiguration
@LazyConfiguration
public class AzInternalMortgagePageData extends InternalMortgagePageData {
    @Override
    public String pageTitle() {
        return  "Daxili İpoteka Krediti Şərtləri, İpoteka Kalkulyatoru ✔";
    }

    @Override
    public String pageHeader() {
        return "Daxili ipoteka krediti";
    }
}
