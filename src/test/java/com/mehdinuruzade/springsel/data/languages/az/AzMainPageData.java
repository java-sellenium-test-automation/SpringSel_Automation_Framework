package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.MainPageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.AzeLangConfiguration;




@AzeLangConfiguration
public class AzMainPageData extends MainPageData {


    @Override
    public String urlSuffix() {
        return "az/ferdi";
    }

    @Override
    public String pageTitle() {
        return "ABB - Müasir Faydalı Universal";
    }
}
