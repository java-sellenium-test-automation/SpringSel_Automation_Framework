package com.mehdinuruzade.springsel.pages;

import com.mehdinuruzade.springsel.data.languages.BasePageData;
import com.mehdinuruzade.springsel.pages.Main.MenuNavigation;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import jakarta.annotation.PostConstruct;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;

public abstract class BasePage {



    @Autowired
    protected WebDriver driver;
    @LazyAutoWired
    protected WebDriverWait wait;
    @LazyAutoWired
    protected Actions actions;
    @LazyAutoWired
    protected MenuNavigation navigation;


    protected BasePageData data;

    protected String pageTitle;

    protected WebElement modalCloseBtn;


    @PostConstruct
    private void init(){
        PageFactory.initElements(this.driver,this);
        driver.manage().window().maximize();
    }


    public String getPageTitle() {
        return this.driver.getTitle();
    }

    public abstract BasePage verifyPage();
    public MenuNavigation navigate(){
        return navigation;
    }


    private boolean isModal(){
        boolean isIt = false;
        try {
            modalCloseBtn = driver.findElement(By.cssSelector("div.js-modal-close"));
            if(modalCloseBtn.isDisplayed() && modalCloseBtn.isEnabled()){
                isIt = true;
            }
        }catch (NoSuchElementException e){
            isIt = false;
        }
        return isIt;
    }



    protected void closeModal(){
        if (isModal()){
            modalCloseBtn.click();
        }

    }

}
