package com.mehdinuruzade.springsel.data.languages.ru;

import com.mehdinuruzade.springsel.data.languages.AbbMobilePageData;


//Currently ABB Bank page forwards from Russian page to Azerbaijani ABB Mobile page , so that this configuration not usable now
//@Configuration
public class RuAbbMobilePageData extends AbbMobilePageData {
    
    @Override
    public String pageTitle() {
        return "ABB mobile";
    }

    @Override
    public String pageHeader() {
        return "ABB mobile";
    }
}


