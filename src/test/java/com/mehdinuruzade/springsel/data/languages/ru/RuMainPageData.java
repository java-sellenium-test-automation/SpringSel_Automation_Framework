package com.mehdinuruzade.springsel.data.languages.ru;

import com.mehdinuruzade.springsel.data.languages.MainPageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.RusLangConfiguration;


@RusLangConfiguration
public class RuMainPageData extends MainPageData {


    @Override
    public String urlSuffix() {
        return "ru/ferdi";
    }

    @Override
    public String pageTitle() {
        return "Банк ABB - Современный, Полезный, Универсальный";
    }
}
