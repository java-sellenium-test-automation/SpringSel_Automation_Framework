package com.mehdinuruzade.springsel.springsel_web.config;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import java.net.URL;

@LazyConfiguration
@Profile("remote")
public class RemoteWebDriverConfig {

    @Value("${selenium.grid.url}")
    private URL gridUrl;

    @Bean
    @ConditionalOnProperty(name = "browser",havingValue = "firefox")
    public WebDriver remoteFireFoxDriver(){
        return new RemoteWebDriver(this.gridUrl, new FirefoxOptions());
    }


    @Bean
   // @ConditionalOnProperty(name = "browser",havingValue = "chrome")
    @ConditionalOnMissingBean
    public WebDriver remoteChromeDriver(){
        return new RemoteWebDriver(this.gridUrl, new ChromeOptions());
    }

}
