package com.mehdinuruzade.springsel.data.languages;



public abstract class AbbMobilePageData extends BasePageData{

    @Override
    public abstract String pageTitle();

    public abstract String pageHeader();
}


