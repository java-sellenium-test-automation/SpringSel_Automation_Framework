package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.CardsPageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.AzeLangConfiguration;

@AzeLangConfiguration
public class AzCardsPageData extends CardsPageData {
    @Override
    public String pageTitle() {
        return "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları";
    }
}
