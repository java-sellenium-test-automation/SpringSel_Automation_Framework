package com.mehdinuruzade.springsel.pages.AbbMobile;

import com.mehdinuruzade.springsel.data.languages.AbbMobilePageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.pages.BasePage;
import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

@Page
public class AbbMobilePage extends BasePage {

    @LazyAutoWired
    private AbbMobilePageData data;




    @FindBy(css="h1")
    private WebElement abbMobileHeader;


    @Override
    public AbbMobilePage verifyPage() {
        closeModal();
        Assert.assertEquals(abbMobileHeader.getText(),data.pageHeader());
        if(abbMobileHeader.getText().equals(data.pageHeader())){
            return this;
        }

        Assert.fail();
        return null;

    }
}
