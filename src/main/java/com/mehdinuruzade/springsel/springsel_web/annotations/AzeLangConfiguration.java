package com.mehdinuruzade.springsel.springsel_web.annotations;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.lang.annotation.*;

@Lazy
@Configuration
@ConditionalOnProperty(value = "app.language",havingValue = "AZE",matchIfMissing = true)
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AzeLangConfiguration {
}
