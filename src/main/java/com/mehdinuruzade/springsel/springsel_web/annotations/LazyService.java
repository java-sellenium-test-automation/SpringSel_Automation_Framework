package com.mehdinuruzade.springsel.springsel_web.annotations;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.lang.annotation.*;

@Service
@Scope(ConfigurableListableBeanFactory.SCOPE_PROTOTYPE)
@Lazy
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LazyService {
}
