package com.mehdinuruzade.springsel.pages.Main;
import com.mehdinuruzade.springsel.data.languages.MainPageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import com.mehdinuruzade.springsel.pages.BasePage;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.testng.Assert;

@Page
public class MainPage extends BasePage {


    @LazyAutoWired
    MainPageData data;


    @Value("${application.url}")
    private String baseUrl;

    private String App() {
        String finalUrl = baseUrl + data.urlSuffix();
        return finalUrl;
    }


    //Methods
    public MainPage goTo(){
        this.driver.get(App());
        return this;
    }


    @Override
    public MainPage verifyPage(){
        closeModal();
        Assert.assertEquals(getPageTitle(),data.pageTitle());
        if(getPageTitle().equals(data.pageTitle())){

            return this;
        }
        return null;
    }










}
