package com.mehdinuruzade.springsel;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;

@SpringBootTest
public class BaseTest extends AbstractTestNGSpringContextTests {

    @BeforeEach
    public void setup() {
    }

    @LazyAutoWired
    public ApplicationContext applicationContext;

    @AfterTest
    public void teardown() {
        WebDriver driver = this.applicationContext.getBean(WebDriver.class);

        driver.close();
        driver.quit();
    }
}
