package com.mehdinuruzade.springsel.pages.Cards;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.PageFragment;
import com.mehdinuruzade.springsel.pages.BaseComponent;
import com.mehdinuruzade.springsel.pages.Card.CardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

@PageFragment
public class ChooseCardComp extends BaseComponent {



//Page instances
    @LazyAutoWired
    private CardPage cardPage;




//Locators
    @FindBy(css = "span[data-hidden-text='Gizlət']")
    private List <WebElement> expandBtns;

    @FindBy(css = "div.row>a[href*='https://abb-bank.az/az/ferdi/kartlar/']")
    private List<WebElement> cards;


//Methods
    public ChooseCardComp expandCardList(){
        expandBtns.get(0).click();
        expandBtns.get(1).click();
        wait.until(ExpectedConditions.visibilityOfAllElements(cards));

        return this;
    }

    public CardPage clickTheCard(List<String> card) {
        closeModal();

        String  locatorValue = String.format("div.row>a[href*='%s']",card.get(0));

        WebElement selectedCard = driver.findElement(By.cssSelector(locatorValue));

        wait.until(ExpectedConditions.elementToBeClickable(selectedCard));
        actions.scrollToElement(selectedCard).build().perform();
        selectedCard.click();

        return cardPage;
    }



}
