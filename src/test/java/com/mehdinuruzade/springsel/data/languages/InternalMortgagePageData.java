package com.mehdinuruzade.springsel.data.languages;



public abstract class InternalMortgagePageData extends BasePageData{

    @Override
    public abstract String pageTitle();

    public abstract String pageHeader();

}


