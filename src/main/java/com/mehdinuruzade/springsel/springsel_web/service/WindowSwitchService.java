package com.mehdinuruzade.springsel.springsel_web.service;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyService;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@LazyService
public class WindowSwitchService {

    @Autowired
    ApplicationContext ctx;

    public void switchByTitle(final String title){
        WebDriver driver = this.ctx.getBean(WebDriver.class);
        driver.getWindowHandles()
                .stream()
                .map(handle -> driver.switchTo().window(handle).getTitle())
                .filter(t -> t.startsWith(title))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No such window: " + title));
    }

    public void switchByWindowId(final int id){
        WebDriver driver = this.ctx.getBean(WebDriver.class);
        String[] windowHandles= driver.getWindowHandles()
                .toArray(new String[0]);

        driver.switchTo().window(windowHandles[id]);
    }

}
