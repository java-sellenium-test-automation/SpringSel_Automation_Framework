package com.mehdinuruzade.springsel.pages.Card;

import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import com.mehdinuruzade.springsel.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.util.List;

@Page
public class CardPage extends BasePage {

    @FindBy(css = "h1")
    private WebElement cardPageHeader;




    @Override
    public CardPage verifyPage() {
        Assert.fail();
        throw new RuntimeException("Please use overloaded method verifyPage(String cardTitle)");
    }

    public CardPage verifyPage(List<String> card) {
        closeModal();
        Assert.assertEquals(getPageTitle(),card.get(1));
        if(getPageTitle().equals(card.get(1))){
            return this;
        }
        Assert.fail();
        return null;
    }

    public  CardPage verifyCard(List<String> card){
        closeModal();

        wait.until(ExpectedConditions.visibilityOf(cardPageHeader));

        if (cardPageHeader.getText().equals(card.get(1))){
            return this;
        }else {
            Assert.fail();
            return null;
        }
    }




}
