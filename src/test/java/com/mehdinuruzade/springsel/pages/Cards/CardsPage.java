package com.mehdinuruzade.springsel.pages.Cards;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import com.mehdinuruzade.springsel.pages.BasePage;
import com.mehdinuruzade.springsel.data.languages.CardsPageData;
import jakarta.annotation.PostConstruct;
import org.testng.Assert;


@Page
public class CardsPage extends BasePage {


    @LazyAutoWired
    private ChooseCardComp chooseCardComp;

    @LazyAutoWired
    private CardsPageData data;

    public ChooseCardComp getChooseCard() {
        return chooseCardComp;
    }


    @Override
    public CardsPage verifyPage(){

        Assert.assertEquals(getPageTitle(),data.pageTitle());
        if(getPageTitle().equals(data.pageTitle())){
            return this;
        }
        return null;
    }



}

