package com.mehdinuruzade.springsel.data.languages;

import java.util.List;


public abstract class Cards {

    public abstract List<String> TAMCARD_MASTERCARD_DEBIT();

    public abstract List<String> TAMCARD_VISACLASSIC_DEBIT();

    public abstract List<String> TAMCJR_VISA_DEBIT();

    public abstract List<String> TAMECO_VISACLASSIC_DEBIT();

    public abstract List<String> TAM_DIGITAL();

    public abstract List<String> TAMCARD_MASTERCARD_GOLD_DEBIT();

    public abstract List<String> TAMCARD_VISA_GOLD_DEBIT();

    public abstract List<String> TAMCARD_MASTERCARD_PLATINUM_DEBIT();

    public abstract List<String> TAMCARD_MASTERCARD_PREMIUM_DEBIT();

    public abstract List<String> TAMCARD_VISA_PREMIUM_DEBIT();

    public abstract List<String> TAMCARD_VISA_CLASSIC();

    public abstract List<String> TAMCARD_MASTERCARD_STANDARD();

    public abstract List<String> TAMCARD_VISA_PLATINUM();

    public abstract List<String> ABB_MILES_VISA_CREDIT();

    public abstract List<String> TAMCARD_MASTERCARD_PLATINUM();

    public abstract List<String> TAMCARD_MASTERCARD_GOLD();

    public abstract List<String> ABB_MILES_VISA_PREMIUM_CREDIT();

    public abstract List<String> ABB_MILES_MASTERCARD_PREMIUM_CREDIT();

}
