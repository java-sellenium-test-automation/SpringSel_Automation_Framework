package com.mehdinuruzade.springsel.springsel_web.service;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyService;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@LazyService
public class FileDownloaderService {

    @LazyAutoWired
    private Resource resource;

    @LazyAutoWired
    private Path path;

    public void downloadFile(Resource resource, Path path) throws IOException {
        FileCopyUtils.copy(resource.getInputStream(), Files.newOutputStream(path));
    }
}
