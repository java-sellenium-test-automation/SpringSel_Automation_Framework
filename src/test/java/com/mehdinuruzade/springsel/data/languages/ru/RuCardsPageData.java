package com.mehdinuruzade.springsel.data.languages.ru;

import com.mehdinuruzade.springsel.data.languages.CardsPageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.RusLangConfiguration;

@RusLangConfiguration
public class RuCardsPageData extends CardsPageData {
    @Override
    public String pageTitle() {
        return "abb-bank.az/ru/ferdi/kartlar";
    }
}
