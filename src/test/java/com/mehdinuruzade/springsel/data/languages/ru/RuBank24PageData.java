package com.mehdinuruzade.springsel.data.languages.ru;

import com.mehdinuruzade.springsel.data.languages.Bank24PageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.RusLangConfiguration;

@RusLangConfiguration
public class RuBank24PageData extends Bank24PageData {
    @Override
    public String pageTitle() {
        return "Банк 24/7";
    }

    @Override
    public String pageHeader() {
        return "Банк 24/7";
    }
}
