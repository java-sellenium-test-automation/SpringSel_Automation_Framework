package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.Bank24PageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.AzeLangConfiguration;

@AzeLangConfiguration
public class AzBank24PageData extends Bank24PageData {
    @Override
    public String pageTitle() {
        return "Bank 24/7";
    }

    @Override
    public String pageHeader() {
        return "Bank 24/7";
    }
}
