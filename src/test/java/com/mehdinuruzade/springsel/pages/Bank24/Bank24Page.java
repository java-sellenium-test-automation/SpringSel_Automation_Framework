package com.mehdinuruzade.springsel.pages.Bank24;

import com.mehdinuruzade.springsel.data.languages.AbbMobilePageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.springsel_web.annotations.Page;
import com.mehdinuruzade.springsel.data.languages.Bank24PageData;
import com.mehdinuruzade.springsel.pages.AbbMobile.AbbMobilePage;
import com.mehdinuruzade.springsel.pages.BasePage;
import com.mehdinuruzade.springsel.springsel_web.service.WindowSwitchService;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.util.List;

@Page
public class Bank24Page extends BasePage {

    @LazyAutoWired
    WindowSwitchService windowSwitcher;

    @LazyAutoWired
    AbbMobilePage abbMobilePage;

    @LazyAutoWired
    private Bank24PageData data;

    @LazyAutoWired
    private AbbMobilePageData abbMobileData;

    @FindBy(css = "h1")
    private WebElement bank24Header;

    @FindBy(css = "div.col-lg-7>div>span>a")
    private List<WebElement> bank24MenuBtns;

    @Override
    public Bank24Page verifyPage() {
        closeModal();
        Assert.assertEquals(bank24Header.getText(),data.pageHeader());
        if(bank24Header.getText().equals(data.pageHeader())){
            return this;
        }

        Assert.fail();
        return null;
    }

    public AbbMobilePage clickABBMobilBank() {
        closeModal();
        WebElement mobilBankBtn = bank24MenuBtns.get(0);
        mobilBankBtn.click();
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        windowSwitcher.switchByTitle(abbMobileData.pageTitle());
        return abbMobilePage;
    }

}
