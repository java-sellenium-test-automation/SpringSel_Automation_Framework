package com.mehdinuruzade.springsel.data.languages;

public abstract class BasePageData {

    public abstract String pageTitle();
}
