package com.mehdinuruzade.springsel.springsel_web.config;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;
import com.github.javafaker.Faker;
import org.springframework.context.annotation.Bean;

@LazyConfiguration
public class FakerConfiguration {

    @Bean
    public Faker faker(){
        return new Faker();
    }
}
