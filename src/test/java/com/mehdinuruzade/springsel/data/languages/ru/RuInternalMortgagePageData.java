package com.mehdinuruzade.springsel.data.languages.ru;

import com.mehdinuruzade.springsel.data.languages.InternalMortgagePageData;


//@RusLangConfiguration
//Configuration cannot be activated due to ABB Bank website redirects from Russian mainpage to Azerbaijani mortgage page
public class RuInternalMortgagePageData extends InternalMortgagePageData {
    @Override
    public String pageTitle() {
        return  "Внутренний ипотечный кредит";
    }

    @Override
    public String pageHeader() {
        return "Внутренний ипотечный кредит";
    }
}
