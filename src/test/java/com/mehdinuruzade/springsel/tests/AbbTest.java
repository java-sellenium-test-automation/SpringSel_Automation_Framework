package com.mehdinuruzade.springsel.tests;

import com.mehdinuruzade.springsel.BaseTest;
import com.mehdinuruzade.springsel.data.languages.Cards;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyAutoWired;
import com.mehdinuruzade.springsel.pages.Cards.CardsPage;
import com.mehdinuruzade.springsel.pages.Main.MainPage;
import org.testng.annotations.Test;

import java.util.List;

public class AbbTest extends BaseTest {


    @LazyAutoWired
    private MainPage mainPage;
    @LazyAutoWired
    private CardsPage cardsPage;

    @LazyAutoWired
    private Cards cards;



    @Test(testName = "Task 1")
    public void checkMainPageTitle(){
        this.mainPage.goTo()
                .verifyPage();
    }

    @Test(testName = "Task 2")
    public void checkCardsPageTitle(){
        mainPage.goTo()
                .verifyPage()
                .navigate()
                .toCardsPage()
                .verifyPage();
    }

    @Test(testName = "Task 3")
    public void chooseCardByName(){
        List<String> card = cards.TAM_DIGITAL();

        mainPage.goTo()
                .verifyPage()
                .navigate()
                .toCardsPage()
                .verifyPage()
                .getChooseCard()
                .expandCardList()
                .clickTheCard(card)
                .verifyPage(card);
    }

      @Test(testName = "Task 4")
      public void checkNewWindowHeader() {
          mainPage.goTo()
                  .verifyPage()
                  .navigate()
                  .toBank24Page()
                  .verifyPage()
                  .clickABBMobilBank()
                  .verifyPage();

      }

      @Test(testName = "Task 5")
      public void checkOptionIsChosen(){
        mainPage.goTo()
                .verifyPage()
                .navigate()
                .toInternalMortgagePage()
                .verifyPage()
                .clickFormNoButton()
                .formNoButtonIsSelected();

      }

}
