package com.mehdinuruzade.springsel.data.languages;



public abstract class CardsPageData extends BasePageData {

    @Override
    public abstract String  pageTitle();

}
