package com.mehdinuruzade.springsel.data.languages.az;

import com.mehdinuruzade.springsel.data.languages.AbbMobilePageData;
import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;

//@AzeLangConfiguration
@LazyConfiguration
public class AzAbbMobilePageData extends AbbMobilePageData {

    @Override
    public String pageTitle() {
        return "ABB mobile – Sadə və sürətli";
    }

    @Override
    public String pageHeader() {
        return "ABB mobile – Sadə və sürətli";
    }
}


