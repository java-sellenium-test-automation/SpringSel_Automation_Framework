package com.mehdinuruzade.springsel.springsel_web.config;

import com.mehdinuruzade.springsel.springsel_web.annotations.LazyConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

@LazyConfiguration
public class ActionsConfig {

    @Autowired
    WebDriver driver;

    @Bean
    public Actions actions(){
        return new Actions(driver);
    }
}
